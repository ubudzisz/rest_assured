package trello.boards;

import trello.common.BaseTest;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import static io.restassured.RestAssured.given;
import static org.apache.http.HttpStatus.SC_OK;
import static org.assertj.core.api.Assertions.assertThat;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class MoveCardBetweenListsTests extends BaseTest {

    @Test
    @Order(1)
    public void createNewBoard() {
        Response response = given()
                .spec(reqSpecification)
                .queryParams("name", "Test Board")
                .queryParam("defaultLists", false)
                .when()
                .post(BASE_URL + BOARDS)
                .then()
                .statusCode(SC_OK)
                .extract()
                .response();

        JsonPath json = response.jsonPath();
        assertThat(json.getString("name")).isEqualTo("Test Board");

        boardID = json.getString("id");
    }

    @Test
    @Order(2)
    public void createFirstList() {
        Response response = given()
                .spec(reqSpecification)
                .queryParams("name", "Test List First")
                .queryParam("idBoard", boardID)
                .when()
                .post(BASE_URL + LISTS)
                .then()
                .statusCode(SC_OK)
                .extract()
                .response();

        JsonPath json = response.jsonPath();
        assertThat(json.getString("name")).isEqualTo("Test List First");

        firstListID = json.getString("id");
    }

    @Test
    @Order(3)
    public void createSecondList() {
        Response response = given()
                .spec(reqSpecification)
                .queryParams("name", "Test List Second")
                .queryParam("idBoard", boardID)
                .when()
                .post(BASE_URL + LISTS)
                .then()
                .statusCode(SC_OK)
                .extract()
                .response();

        JsonPath json = response.jsonPath();
        assertThat(json.getString("name")).isEqualTo("Test List Second");

        secondListID = json.getString("id");
    }

    @Test
    @Order(4)
    public void createCardForFirstList() {
        Response response = given()
                .spec(reqSpecification)
                .queryParam("name", "First Card")
                .queryParam("idList", firstListID)
                .when()
                .post(BASE_URL + CARDS)
                .then()
                .statusCode(SC_OK)
                .extract()
                .response();

        JsonPath json = response.jsonPath();
        assertThat(json.getString("name")).isEqualTo("First Card");

        cardListID = json.getString("id");
    }

    @Test
    @Order(5)
    public void moveCardForSecondList() {
        Response response = given()
                .spec(reqSpecification)
                .queryParam("name", "First Card")
                .queryParam("idList", secondListID)
                .when()
                .put(BASE_URL + CARDS + "/" + cardListID)
                .then()
                .statusCode(SC_OK)
                .extract()
                .response();

        JsonPath json = response.jsonPath();
        assertThat(json.getString("idList")).isEqualTo(secondListID);
    }

    @Test
    @Order(6)
    public void deleteBoard() {
        given()
                .spec(reqSpecification)
                .when()
                .delete(BASE_URL + BOARDS + "/" + boardID)
                .then()
                .statusCode(SC_OK);
    }
}

