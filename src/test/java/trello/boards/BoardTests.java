package trello.boards;

import trello.common.BaseTest;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;


public class BoardTests extends BaseTest {
    @Test
    @Order(1)
    public void createNewBoard() {
        Response response = given()
                .spec(reqSpecification)
                .queryParams("name", "newBoard")
                .when()
                .post(BASE_URL + BOARDS)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();

        JsonPath json = response.jsonPath();
        assertThat(json.getString("name"))
                .isEqualTo("newBoard");

        String boardID = json.get("id");

        given()
                .spec(reqSpecification)
                .when()
                .delete(BASE_URL + BOARDS + "/" + boardID)
                .then()
                .statusCode(HttpStatus.SC_OK);

    }

    @Test
    @Order(2)
    public void createBoardWithEmptyName() {
        Response response = given()
                .spec(reqSpecification)
                .queryParams("name", "")
                .contentType(ContentType.JSON)
                .when()
                .post(BASE_URL + BOARDS)
                .then()
                .statusCode(400)
                .extract()
                .response();
    }

    @Test
    @Order(3)
    public void createBoardWithoutDefaultList() {
        Response response = given()
                .spec(reqSpecification)
                .queryParam("name", "Board Without List")
                .queryParam("defaultLists", false)
                .when()
                .post(BASE_URL + BOARDS)
                .then()
                .statusCode(200)
                .extract()
                .response();

        JsonPath json = response.jsonPath();
        assertThat(json.getString("name"))
                .isEqualTo("Board Without List");
        String boardID = json.get("id");

        Response response1 = given()
                .spec(reqSpecification)
                .queryParam("name", "Board Without List")
                .queryParam("defaultLists", false)
                .when()
                .get(BASE_URL + BOARDS + "/" + boardID + LISTS)
                .then()
                .statusCode(200)
                .extract()
                .response();

        JsonPath jsonList = response1.jsonPath();
        List<String> idList = jsonList.getList("id");
        assertThat(idList).hasSize(0);

        given()
                .spec(reqSpecification)
                .when()
                .delete(BASE_URL + BOARDS + "/" + boardID)
                .then()
                .statusCode(HttpStatus.SC_OK);
    }

    @Test
    @Order(4)
    public void createBoardWithDefaultList() {
        Response response = given()
                .spec(reqSpecification)
                .queryParam("name", "Board With List")
                .queryParam("defaultLists", true)
                .when()
                .post(BASE_URL + BOARDS)
                .then()
                .statusCode(200)
                .extract()
                .response();

        JsonPath jsonList2 = response.jsonPath();

        assertThat(jsonList2.getString("name")).isEqualTo("Board With List");
        String boardID = jsonList2.get("id");

        Response response1 = given()
                .spec(reqSpecification)
                .queryParam("name", "Board With List")
                .queryParam("defaultLists", false)
                .when()
                .get(BASE_URL + BOARDS + "/" + boardID + LISTS)
                .then()
                .statusCode(200)
                .extract()
                .response();

        JsonPath jsonList3 = response1.jsonPath();
        List<String> nameList = jsonList3.getList("name");
        assertThat(nameList)
                .hasSize(3)
                .contains("To Do", "Doing", "Done");

        given()
                .spec(reqSpecification)
                .when()
                .delete(BASE_URL + BOARDS + "/" + boardID)
                .then()
                .statusCode(HttpStatus.SC_OK);
    }

}


