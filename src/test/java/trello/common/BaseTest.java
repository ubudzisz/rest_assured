package trello.common;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeAll;

public class BaseTest {
    protected static final String KEY = "";
    protected static final String TOKEN = "";
    protected static final String BASE_URL = "https://api.trello.com/1";
    protected static final String BOARDS = "/boards";
    protected static final String LISTS = "/lists";
    protected static final String CARDS = "/cards";
    protected static final String ORGANIZATIONS = "/organizations";

    protected static String boardID;
    protected static String firstListID;
    protected static String secondListID;
    protected static String cardListID;

    protected static RequestSpecBuilder reqBuilder;
    protected static RequestSpecification reqSpecification;

    @BeforeAll
    public static void RequestSpecBuilder() {
        reqBuilder = new RequestSpecBuilder();
        reqBuilder.addQueryParam("key", KEY);
        reqBuilder.addQueryParam("token", TOKEN);
        reqBuilder.setContentType(ContentType.JSON);

        reqSpecification = reqBuilder.build();
    }
}

