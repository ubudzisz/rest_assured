package trello.organizations;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import trello.common.BaseTest;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static io.restassured.RestAssured.given;
import static org.apache.http.HttpStatus.SC_OK;
import static org.assertj.core.api.Assertions.assertThat;

public class OrganizationParametersTests extends BaseTest {

    private static Stream<Arguments> createOrganizationData() {
        return Stream.of(
                Arguments.of("Display name","This is description","This is name", "https://trello.com/" ),
                Arguments.of("Display name","This is description","This is name", "http://trello.com/" ),
                Arguments.of("Display name","This is description","1234", "http://trello.com/" ),
                Arguments.of("Display name","This is description","23", "http://trello.com/" ),
                Arguments.of("Display name","This is description","nam", "http://trello.com/" ),
                Arguments.of("Display name","This is description","This is name", "trello.com/" ));
    }


    private static Stream<Arguments> createOrganizationInvalidData() {
        return Stream.of(
                Arguments.of("Display name","This is ","The", "lorem ipsum" ),
                Arguments.of("","This is description","This is name", "http://trello.com/" ),
                Arguments.of("Display name","Description","12", "" ),
                Arguments.of("23","23","&&", "https://trello.com/" ),
                Arguments.of("&&","__", "AAA", "trello" ));
    }

    @DisplayName("Create organization with valid data")
    @ParameterizedTest(name = "Display name: {0}, desc: {1}, name: {2}, website: {3}")
    @MethodSource("createOrganizationData")
    public void createOrganization(String displayName, String desc, String name, String website) {

        Response response = given()
                .spec(reqSpecification)
                .queryParam("displayName", displayName)
                .queryParam("desc", desc)
                .queryParam("name", name)
                .queryParam("website", website )
                .when()
                .post(BASE_URL + ORGANIZATIONS)
                .then()
                .statusCode(SC_OK)
                .extract()
                .response();

        JsonPath json = response.jsonPath();
        assertThat(json.getString("displayName")).isEqualTo(displayName);

        final String organizationID = json.getString("id");

        given()
                .spec(reqSpecification)
                .when()
                .delete(BASE_URL + ORGANIZATIONS + "/" + organizationID)
                .then()
                .statusCode(SC_OK);
    }

    @DisplayName("Create organization with invalid data")
    @ParameterizedTest(name = "Display name: {0}, desc: {1}, name: {2}, website: {3}")
    @MethodSource("createOrganizationInvalidData")
    public void createOrganizationWithInvalidData(String displayName, String desc, String name, String website) {

       List<String> organizationInvalidIDs = new ArrayList();

        Response response = given()
                .spec(reqSpecification)
                .queryParam("displayName", displayName)
                .queryParam("desc", desc)
                .queryParam("name", name)
                .queryParam("website", website)
                .when()
                .post(BASE_URL + ORGANIZATIONS)
                .then()
                .extract()
                .response();

        JsonPath json = response.jsonPath();

        if (response.getStatusCode() == 200) organizationInvalidIDs.add(json.getString("id"));
        final String organizationName = json.getString("displayName");

        for (String orgID : organizationInvalidIDs) {
            given()
                    .spec(reqSpecification)
                    .when()
                    .delete(BASE_URL + ORGANIZATIONS + "/" + orgID)
                    .then()
                    .statusCode(SC_OK);
            System.out.println("Organization " + organizationName + " was created and deleted. Please validate incorrect API behaviour");
        }

        assertThat(response.getStatusCode()).isEqualTo(400);
    }
}
