package trello.organizations;

import com.github.javafaker.Faker;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.jupiter.api.*;
import trello.common.BaseTest;

import static io.restassured.RestAssured.given;
import static org.apache.http.HttpStatus.SC_BAD_REQUEST;
import static org.apache.http.HttpStatus.SC_OK;
import static org.assertj.core.api.Assertions.assertThat;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)

public class OrganizationTests extends BaseTest {
    protected static String organizationFirstID;
    protected static String organizationSecondID;
    protected static String idMember;

    private static Faker faker;
    private String fakeName;
    private String fakeWebsite;
    private String fakeDesc;
    private String fakeDisplayName;
    private String fakeMemberEmail;
    private String fakeMemberFullName;


    @BeforeAll
    public static void beforeAll() {
        faker = new Faker();
    }

    @BeforeEach
    public void beforeEach() {
        fakeName = faker.beer().name();
        fakeDesc = faker.howIMetYourMother().quote();
        fakeWebsite = faker.internet().url();
        fakeDisplayName = faker.funnyName().name();
        fakeMemberEmail = faker.internet().emailAddress();
        fakeMemberFullName = faker.name().fullName();

    }

    @Test
    @Order(1)
    public void createFirstOrganization() {
        Response response = given()
                .spec(reqSpecification)
                .queryParam("displayName", fakeDisplayName)
                .queryParam("name", fakeName)
                .queryParam("desc", fakeDesc)
                .queryParam("website", fakeWebsite )
                .when()
                .post(BASE_URL + ORGANIZATIONS)
                .then()
                .statusCode(SC_OK)
                .extract()
                .response();

        JsonPath json = response.jsonPath();
        assertThat(json.getString("displayName")).isEqualTo(fakeDisplayName);

        organizationFirstID = json.get("id");

    }

    @Test
    @Order(2)
    public void createOrganizationWithoutDisplayName() {
        Response response = given()
                .spec(reqSpecification)
                .queryParam("displayName", "")
                .queryParam("name", fakeName)
                .queryParam("desc", fakeDesc)
                .queryParam("website", fakeWebsite )
                .when()
                .post(BASE_URL + ORGANIZATIONS)
                .then()
                .statusCode(SC_BAD_REQUEST)
                .extract()
                .response();
    }

    @Test
    @Order(3)
    public void addMembersToFirstOrganization() {

        Response response = given()
                .spec(reqSpecification)
                .queryParam("email", fakeMemberEmail )
                .queryParam("fullName", fakeMemberFullName )
                .queryParam("type", "normal" )
                .queryParam("id", organizationFirstID)
                .when()
                .put(BASE_URL + ORGANIZATIONS + "/" + organizationFirstID + "/members")
                .then()
                .statusCode(SC_OK)
                .extract()
                .response();

        JsonPath json = response.jsonPath();
        assertThat(json.getString("members[0].fullName")).isEqualTo(fakeMemberFullName);
        idMember = json.getString("memberships[0].idMember");
    }


    @Test
    @Order(4)
    public void createSecondOrganization() {

        Response response = given()
                .spec(reqSpecification)
                .queryParam("displayName", fakeDisplayName)
                .queryParam("name", "%#")
                .queryParam("desc", fakeDesc)
                .queryParam("website", "fakeWebsite" )
                .when()
                .post(BASE_URL + ORGANIZATIONS)
                .then()
                .statusCode(SC_OK)
                .extract()
                .response();

        JsonPath json = response.jsonPath();
        assertThat(json.getString("displayName")).isEqualTo(fakeDisplayName);
        organizationSecondID = json.get("id");
    }

    @Test
    @Order(5)
    public void deleteFirstOrganization() {

        given()
                .spec(reqSpecification)
                .when()
                .delete(BASE_URL + ORGANIZATIONS + "/" + organizationFirstID)
                .then()
                .statusCode(SC_OK)
                .extract()
                .response();
    }

    @Test
    @Order(6)
    public void deleteSecondOrganization() {
        given()
                .spec(reqSpecification)
                .when()
                .delete(BASE_URL + ORGANIZATIONS + "/" + organizationSecondID)
                .then()
                .statusCode(SC_OK)
                .extract()
                .response();

    }

}


